/**
 * 
 */

package ru.mephi.dozen.hw1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import ru.mephi.dozen.hw1.BidCount.BidMapper;
/**
 * @author Айнур.
 *
 */

public class MapperTest {
  MapDriver<Object, Text, LogWritableComparable, LogWritable> mapDriver;

  @Before
  public void setUp() {
    BidMapper mapper = new BidMapper();
    mapDriver = MapDriver.newMapDriver(mapper);
  }

  @Test
  public void testMapperDefault() throws IOException {
    // Good example
    mapDriver.withInput(new LongWritable(), new Text("481a3572b254fac0a100b3264ae0dff0  "
        + "20131027105900443  1  CC3FELF7dBw  Mozilla/5.0 (Windows NT 5.1) "
        + "AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE "
        + "2.X MetaSr 1.0  112.251.126.*  146  159  2  13625cb070ffb306b425cd803c4b7ab4  "
        + "d2a55a0aaf1e75231290f9369b711aa1  null  127598132  728  90  OtherView  Na  "
        + "133  12627  277  133  null  2261  13800,10117,10102,10024,10006,10110,10133,10063\r\n"
        + ""));
    mapDriver.withOutput(new LogWritableComparable(159, "(Windows NT 5.1)"), new LogWritable(1));
    mapDriver.runTest();
  }

  @Test
  public void testMapperIncorrectCityId() throws IOException {
    // cityId incorrect format
    mapDriver.withInput(new LongWritable(), new Text("481a3572b254fac0a100b3264ae0dff0  "
        + "20131027105900443  1  CC3FELF7dBw  Mozilla/5.0 (Windows NT 5.1) "
        + "AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE "
        + "2.X MetaSr 1.0  112.251.126.*  146  sa  2  13625cb070ffb306b425cd803c4b7ab4  "
        + "d2a55a0aaf1e75231290f9369b711aa1  null  127598132  728  90  OtherView  Na  "
        + "133  12627  277  133  null  2261  13800,10117,10102,10024,10006,10110,10133,10063\r\n" 
        + ""));
    mapDriver.runTest();
    assertEquals("Expected 1 counter increment", 1, mapDriver.getCounters()
        .findCounter(CustomCounter.CITY_ID_FORMAT).getValue());
  }

  @Test
  public void testMapperLowBidPrice() throws IOException {
    // Bid price lower than minimum
    mapDriver.withInput(new LongWritable(), new Text("481a3572b254fac0a100b3264ae0dff0  "
        + "20131027105900443  1  CC3FELF7dBw  Mozilla/5.0 (Windows NT 5.1) "
        + "AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE "
        + "2.X MetaSr 1.0  112.251.126.*  146  159  2  13625cb070ffb306b425cd803c4b7ab4  "
        + "d2a55a0aaf1e75231290f9369b711aa1  null  127598132  728  90  OtherView  Na  "
        + "133  12627  0  133  null  2261  13800,10117,10102,10024,10006,10110,10133,10063\r\n" 
        + ""));
    mapDriver.runTest();
  }

  @Test
  public void testMapperNoOsType() throws IOException {
    // cityId incorrect format
    mapDriver.withInput(new LongWritable(), new Text("481a3572b254fac0a100b3264ae0dff0  "
        + "20131027105900443  1  CC3FELF7dBw  Mozilla/5.0  "
        + "AppleWebKit/535.11 KHTML, like Gecko Chrome/17.0.963.84 Safari/535.11 SE "
        + "2.X MetaSr 1.0  112.251.126.*  146  192  2  13625cb070ffb306b425cd803c4b7ab4  "
        + "d2a55a0aaf1e75231290f9369b711aa1  null  127598132  728  90  OtherView  Na  "
        + "133  12627  277  133  null  2261  13800,10117,10102,10024,10006,10110,10133,10063\r\n" 
        + ""));
    mapDriver.withOutput(new LogWritableComparable(192, " "), new LogWritable(1));
    mapDriver.runTest();
    assertEquals("Expected 1 counter increment", 1, mapDriver.getCounters()
        .findCounter(CustomCounter.NO_OS_TYPE).getValue());
  }
  
  @Test
  public void testMapperTooShortLine() throws IOException {
    // cityId incorrect format
    mapDriver.withInput(new LongWritable(), new Text("481a3572b254fac0a100b3264ae0dff0"));
    mapDriver.runTest();
    assertEquals("Expected 1 counter increment", 1, mapDriver.getCounters()
        .findCounter(CustomCounter.LINE_LENGTH).getValue());
  }
}
