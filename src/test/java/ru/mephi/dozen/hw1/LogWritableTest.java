/**
 * 
 */

package ru.mephi.dozen.hw1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

/**
 * @author Айнур.
 *
 */
public class LogWritableTest {
  
  @Test
  public void constructorTest() {
    LogWritable l = new LogWritable(10);
    assertEquals(l.get(), 10);
  }
  
  @Test
  public void setTest() {
    LogWritable l = new LogWritable(10);
    l.set(50);
    assertEquals(l.get(), 50);
  }
  
  @Test
  public void equalsTest() {
    LogWritable l = new LogWritable(10);
    LogWritable m = new LogWritable(10);
    LogWritable n = new LogWritable();
    assertEquals(l.equals(m), true);
    assertEquals(l.equals(n), false);
    assertEquals(l.equals(new Object()), false);
  }
  
  @Test
  public void hashCodeTest() {
    LogWritable l = new LogWritable(10);
    LogWritable m = new LogWritable(10);
    LogWritable n = new LogWritable();
    assertEquals(l.hashCode(), m.hashCode());
    assertNotEquals(l.hashCode(), n.hashCode());
  }
}
