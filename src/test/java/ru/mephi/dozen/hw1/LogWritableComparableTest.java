/**
 * 
 */

package ru.mephi.dozen.hw1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

/**
 * @author Айнур.
 *
 */
public class LogWritableComparableTest {
  
  @Test
  public void constructorTest() {
    LogWritableComparable l = new LogWritableComparable(100, "Windows");
    assertEquals(l.get(), 100);
    assertEquals(l.getOperatingSystem(), "Windows");
  }
  
  @Test
  public void setTest() {
    LogWritableComparable l = new LogWritableComparable(100, "Windows");
    l.set(500);
    assertEquals(l.get(), 500);
    l.set(100, "Android");
    assertEquals(l.get(), 100);
    assertEquals(l.getOperatingSystem(), "Android");
  }
  
  @Test
  public void equalsTest() {
    LogWritableComparable l = new LogWritableComparable(10, "Linux");
    LogWritableComparable m = new LogWritableComparable(10, "Linux");
    LogWritableComparable n = new LogWritableComparable();
    assertEquals(l.equals(m), true);
    assertEquals(l.equals(n), false);
    assertEquals(l.equals(new Object()), false);
  }
  
  @Test
  public void hashCodeTest() {
    LogWritableComparable l = new LogWritableComparable(10, "Linux");
    LogWritableComparable m = new LogWritableComparable(10, "Linux");
    LogWritableComparable n = new LogWritableComparable();
    assertEquals(l.hashCode(), m.hashCode());
    assertNotEquals(l.hashCode(), n.hashCode());
  }
  
  @Test
  public void compareToTest() {
    LogWritableComparable l = new LogWritableComparable(10, "Linux");
    LogWritableComparable m = new LogWritableComparable(100, "Linux");
    LogWritableComparable n = new LogWritableComparable(5, "Linux");
    assertEquals(l.compareTo(m), -1);
    assertEquals(l.compareTo(n), 1);
    assertEquals(l.compareTo(l), 0);
    
  }
}
