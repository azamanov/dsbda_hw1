/**
 * 
 */

package ru.mephi.dozen.hw1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import ru.mephi.dozen.hw1.BidCount.BidReduce;

/**
 * @author Айнур.
 *
 */
public class ReducerTest {
  ReduceDriver<LogWritableComparable, LogWritable, Text, LogWritable> reduceDriver;


  @Before
  public void setUp() {
    BidReduce reducer = new BidReduce();
    reduceDriver = ReduceDriver.newReduceDriver(reducer);
  }

  @Test
  public void testReducerDefault() throws IOException {
    List<LogWritable> values = new ArrayList<LogWritable>();
    values.add(new LogWritable(5));
    values.add(new LogWritable(1));
    reduceDriver.withCacheFile("../input/city.en.txt").withCacheFile("../input/region.en.txt")
      .withInput(new LogWritableComparable(192, "(Windows NT 5.1)"), values);
    reduceDriver.withOutput(new Text("jingzhou"), new LogWritable(6));
    reduceDriver.runTest();
  }
  
  @Test
  public void testReducerNoCityName() throws IOException {
    List<LogWritable> values = new ArrayList<LogWritable>();
    values.add(new LogWritable(5));
    values.add(new LogWritable(1));
    reduceDriver.withCacheFile("../input/city.en.txt").withCacheFile("../input/region.en.txt")
      .withInput(new LogWritableComparable(561, "(Windows NT 5.1)"), values);
    reduceDriver.withOutput(new Text("noname"), new LogWritable(6));
    reduceDriver.runTest();
  }
}
