package ru.mephi.dozen.hw1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * LogWritable.java
 * Purpose: Custom data type for values
 * @author Айнур
 *
 */
public class LogWritable implements Writable {

  private int count;

  /* (non-Javadoc)
   * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
   */
  @Override
  public void readFields(DataInput in) throws IOException {
    // TODO Auto-generated method stub
    count = in.readInt();
  }

  /* (non-Javadoc)
   * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
   */
  @Override
  public void write(DataOutput out) throws IOException {
    // TODO Auto-generated method stub
    out.writeInt(count);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + count;
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof LogWritable)) {
      return false;
    }
    LogWritable other = (LogWritable) obj;
    if (count != other.count) {
      return false;
    }
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return String.valueOf(count);
  }

  /**
   * Constructor for LogWritable.
   * 
   * @param count an integer 
   */
  public LogWritable(int count) {
    this.count = count;
  }

  /**
   * Get method for LogWritable.
   * 
   * @return count an integer, private value LogWritable
   */
  public int get() {
    // TODO Auto-generated method stub
    return count;
  }

  /**
   * Default constructor for LogWritable.
   */
  public LogWritable() {
  }

  public void set(int count) {
    // TODO Auto-generated method stub
    this.count = count;
  }

}
