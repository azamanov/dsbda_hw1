package ru.mephi.dozen.hw1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * LogWritableComparable.java
 * Purpose: Custom data type for keys
 * @author Айнур
 *
 */
public class LogWritableComparable implements WritableComparable<LogWritableComparable> {

  private int cityId;
  private String operatingSystem;

  /* (non-Javadoc)
   * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
   */
  @Override
  public void readFields(DataInput in) throws IOException {
    // TODO Auto-generated method stub
    cityId = in.readInt();
    operatingSystem = in.readUTF();
  }

  /* (non-Javadoc)
   * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
   */
  @Override
  public void write(DataOutput out) throws IOException {
    // TODO Auto-generated method stub
    out.writeInt(cityId);
    out.writeUTF(operatingSystem);
  }

  /**
   * Default constructor for LogWritableComparable.
   */
  public LogWritableComparable() {
    // TODO Auto-generated constructor stub
  }

  /** 
   * @param o is LogWritableComparable instance.
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(LogWritableComparable o) {
    // TODO Auto-generated method stub
    int thisValue = this.cityId;
    int thatValue = o.cityId;
    return thisValue < thatValue ? -1 : 
      (thisValue == thatValue ? 0 : 1);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + cityId;
    result = prime * result + ((operatingSystem == null) ? 0 : operatingSystem.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "LogWritableComparable [cityId=" + cityId + ", operatingSystem=" + operatingSystem + "]";
  }

  /**
   * @return An integer with city id.
   */
  public int get() {
    // TODO Auto-generated method stub
    return cityId;
  }


  /**
   * Method for setting private values.
   * @param cityId an Integer.
   * @param operatingSystem a String
   */
  public void set(int cityId, String operatingSystem) {
    this.cityId = cityId;
    this.operatingSystem = operatingSystem;
  }

  /**
   * Method for setting private value.
   * 
   * @param cityId an integer
   */
  public void set(int cityId) {
    this.cityId = cityId;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof LogWritableComparable)) {
      return false;
    }
    LogWritableComparable other = (LogWritableComparable) obj;
    if (cityId != other.cityId) {
      return false;
    }
    if (operatingSystem == null) {
      if (other.operatingSystem != null) {
        return false;
      }
    } else if (!operatingSystem.equals(other.operatingSystem)) {
      return false;
    }
    return true;
  }

  /**
   * @param cityId an Integer.
   * @param operatingSystem a String.
   */
  public LogWritableComparable(int cityId, String operatingSystem) {
    this.cityId = cityId;
    this.operatingSystem = operatingSystem;
  }

  /**
   * @return operating system name.
   */
  public String getOperatingSystem() {
    return operatingSystem;
  }
}
