package ru.mephi.dozen.hw1;

/**
 * MyCounter.java
 * Purpose: Enum for Counters that used for statistics about malformed rows collection
 * 
 * @author Айнур
 */
public enum CustomCounter {
  CITY_ID_FORMAT, LINE_LENGTH, CITY_FILE_FORMAT, MISSING_CITY_NAME, NO_OS_TYPE
}
