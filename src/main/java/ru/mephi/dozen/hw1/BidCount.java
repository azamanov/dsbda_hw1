/**
 * 
 */

package ru.mephi.dozen.hw1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


/**
 * BidCount.java
 * Purpose: calculate amount of high-bid-priced (more than 250) impression events by city
 * 
 * @author Айнур
 * @version 1.0
 */
public class BidCount extends Configured implements Tool {

  public static class BidMapper extends Mapper<Object, Text, LogWritableComparable, LogWritable> {
    private LogWritable one = new LogWritable(1);
    private LogWritableComparable selectedKey = new LogWritableComparable();
    private static final int MIN_BID_PRICE = 250;
    private static final int CITY_OFFSET = 17;
    private static final int BID_OFFSET = 5;
    private Pattern pattern;

    /**
     * Prepare actions for map() function.
     */
    @Override
    public void setup(Context context) throws IOException {
      pattern = Pattern.compile("\\((.*?)\\)");
    }

    /**
     * Maps input key/value pairs to a set of intermediate key/value pairs.
     * 
     */
    public void map(Object key, Text value, Context context) 
        throws IOException, InterruptedException {
      String[] lines = value.toString().split("\\s+");
      if (lines.length < CITY_OFFSET) {
        context.getCounter(CustomCounter.LINE_LENGTH).increment(1);
        return;
      }
      try {
        int cityId = Integer.parseInt(lines[lines.length - CITY_OFFSET]);
        int bidPrice = Integer.parseInt(lines[lines.length - BID_OFFSET]);
        String operatingSystem = null;
        Matcher m = pattern.matcher(value.toString());
        if (m.find()) {
          operatingSystem = m.group();
        } else {
          operatingSystem = " ";
          context.getCounter(CustomCounter.NO_OS_TYPE).increment(1);
        }
        if (bidPrice > MIN_BID_PRICE) {      
          selectedKey.set(cityId, operatingSystem);
          context.write(selectedKey, one);
        }
      } catch (NumberFormatException e) {
        context.getCounter(CustomCounter.CITY_ID_FORMAT).increment(1);
      }
    }
  }

  /**
   * Custom Partitioner by OperationSystemType.
   */
  public static class BidPartitioner extends Partitioner<LogWritableComparable,LogWritable> {

    /**
     * Partitions the key space by operating system type.
     */
    public int getPartition(LogWritableComparable key, LogWritable value, int numReduceTasks) {
      if (numReduceTasks == 0) {
        return 0;
      }
      return (key.getOperatingSystem().hashCode() & Integer.MAX_VALUE) % numReduceTasks;
    }
  }

  public static class BidReduce extends Reducer<LogWritableComparable, LogWritable, 
      Text, LogWritable> {

    HashMap<Integer, String> cityMap = new HashMap<Integer, String>();
    private LogWritable count = new LogWritable();
    
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

      // Get side data from the distributed cache
      URI[] cacheFilesLocal = context.getCacheFiles();

      for (URI eachUri : cacheFilesLocal) {
        if (eachUri.getPath().toUpperCase().contains("CITY") 
            || (eachUri.getPath().toUpperCase().contains("REGION"))) {
          FileSystem fs = FileSystem.get(context.getConfiguration());
          BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(new Path(eachUri))));
          try {
            String line = null;
            while ((line = br.readLine()) != null) {
              String[] lines = line.split("\\s+");
              cityMap.put(Integer.parseInt(lines[0]), lines[1]);
            }
          } catch (NullPointerException e) {
            context.getCounter(CustomCounter.CITY_FILE_FORMAT).increment(1);
          } finally {
            if (br != null) {
              br.close();
            }
          }
        }
      }
    }

    /**
     * Reduces a set of intermediate values which share a key to a smaller set of values.
     */
    public void reduce(LogWritableComparable key, Iterable<LogWritable> values, Context context) 
        throws IOException, InterruptedException {
      int sum = 0;
      for (LogWritable v:values) {
        sum += v.get();
      }
      Integer id = key.get();
      String s = cityMap.get(id);
      Text text;
      if (s != null) {
        text = new Text(s);
      } else {
        text = new Text("noname");
        context.getCounter(CustomCounter.MISSING_CITY_NAME).increment(1);
      }
      count.set(sum);
      context.write(text, count);
    }  
  }

  /**
   * Combiner.
   */
  public static class BidCountCombiner extends Reducer<LogWritableComparable, LogWritable, 
      LogWritableComparable, LogWritable> {

    private LogWritable count = new LogWritable();
    
    /**
     *  Purpose: summarize the map output records with the same key.
     */
    public void reduce(LogWritableComparable key, Iterable<LogWritable> values, Context context)
        throws IOException, InterruptedException {
      int sum = 0;
      for (LogWritable v:values) {
        sum += v.get();
      }
      count.set(sum);
      context.write(key, count);
    }
  }

  /**
   * @param args command-line arguments.
   * @throws Exception any exception.
   */
  public static void main(String[] args) throws Exception {
    // TODO Auto-generated method stub
    int res = ToolRunner.run(new Configuration(), new BidCount(), args);
    System.exit(res);
  }

  /** 
   * Runs the Tool with its Configuration.
   */
  @Override
  public int run(String[] args) throws Exception {
    // TODO Auto-generated method stub
    Configuration conf = this.getConf();
    // Creating a job
    Job job = Job.getInstance(conf, "bidcount");

    // Specify a key/value
    job.setMapOutputKeyClass(LogWritableComparable.class);
    job.setMapOutputValueClass(LogWritable.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(LogWritable.class);

    // Setup MR job
    job.setMapperClass(BidMapper.class);
    job.setCombinerClass(BidCountCombiner.class);
    job.setPartitionerClass(BidPartitioner.class);
    job.setReducerClass(BidReduce.class);

    // Specify input/output paths
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    // Set output format as SequenceFile with Snappy encoding
    FileOutputFormat.setCompressOutput(job, true);
    FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    SequenceFileOutputFormat.setOutputCompressionType(job,CompressionType.BLOCK);

    job.addCacheFile(new URI("/zamanov/input/city.en.txt"));
    job.addCacheFile(new URI("/zamanov/input/region.en.txt"));
    // Execute job
    job.waitForCompletion(true);
    // Output statistics about malformed rows collection
    if (job.isSuccessful()) {
      Counters cn = job.getCounters();
      System.out.println("Amount of missing OS type in dataset: " 
          + cn.findCounter(CustomCounter.NO_OS_TYPE).getValue());
      System.out.println("Amount of missing city or region id in dictionaries: " 
          + cn.findCounter(CustomCounter.MISSING_CITY_NAME).getValue());
      System.out.println("Amount of incorrect city name in dictionaries: " 
          + cn.findCounter(CustomCounter.CITY_FILE_FORMAT).getValue());
      System.out.println("Amount of short lines in dataset: " 
          + cn.findCounter(CustomCounter.LINE_LENGTH).getValue());
      System.out.println("Amount of incorrect cityId in dataset: " 
          + cn.findCounter(CustomCounter.CITY_ID_FORMAT).getValue());    
    } else {
      return 1;
    }
    return 0;
  }
}
